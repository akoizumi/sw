DESTDIR ?=
PREFIX ?= /usr/local

install:
	@echo "Installing..."
	install -Dm0644 sw.1 $(DESTDIR)$(PREFIX)/share/man/man1/sw.1
	install -Dm0644 swrc.5 $(DESTDIR)$(PREFIX)/share/man/man5/swrc.5
	install -m0755 sw $(DESTDIR)$(PREFIX)/bin/sw
uninstall:
	@echo "Uninstalling..."
	rm -f $(DESTDIR)$(PREFIX)/bin/sw
	rm -f $(DESTDIR)$(PREFIX)/share/man/man1/sw.1
	rm -f $(DESTDIR)$(PREFIX)/share/man/man5/swrc.5
