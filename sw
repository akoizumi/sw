#!/bin/sh
cfg=${XDG_CONFIG_HOME:-$HOME/.config}/sw

# Ensure the configuration directory exists
test -d "$cfg" || mkdir -p -- "$cfg"

exec "${@:-$cfg/swrc}"
